# Mock Bank Core Service

This service serves as a mock replacement for banking core web services such as Fiserv, Symitar, & Keystone.

### Build
To build this service, run 
```shell
mvn clean install
```
This will install all necessary dependencies specified in the `pom.xml` 
and build the JAR artifact in the `target` folder.
It will also run the unit tests and run a checkstyle check.
If necessary, they can be disabled with:
```shell
mvn clean install -DskipTests -Dcheckstyle.skip=true
```

#### Integration Tests
You can also run integration tests while building the service by using the `it`  maven profile and an additional parameter for a profile with a database config:
```shell
mvn clean install -P it,local
```

The failsafe plugin will then run any test classes ending in IT (i.e. `*IT.java`)

These tend to be slower to run than unit tests and do not need to be run every time you build the project.

### Run
To start this service, run 
```shell
mvn spring-boot:run
```

This will build the service and start up this Spring Boot Application. By default, the local profile is active
The endpoints can then be accessed at http://localhost:10000.
The port is configured in `src/main/resources/application.yml`

### Databases
By default, this service will use an H2 in-memory database.

The dev profile connect to a remote `mysql` database for the dev env. You can start up the service with the `dev` maven profile:
```shell
mvn spring-boot:run -P dev 
```
The service will then try to create the necessary schema. 
This is configured by `spring.jpa.hibernate.ddl-auto` property

### Deployment
Due to the way the database maven profiles and spring-boot config files are set up, 
you must specify the mysql profile at both build time (to get the dependency), 
and at runtime (to activate the profile). 

##### Build
```shell
mvn clean install -P dev
```
##### Run
```shell
java -jar mockcore.jar --spring.profiles.active=dev
```
