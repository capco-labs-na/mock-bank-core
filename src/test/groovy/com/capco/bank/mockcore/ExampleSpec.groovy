package com.capco.bank.mockcore

import com.capco.bank.mockcore.config.BootstrapExampleProperties
import com.capco.bank.mockcore.repository.MockCoreRepository
import com.capco.bank.mockcore.repository.model.ExampleEntity
import spock.lang.Specification

class ExampleSpec extends Specification {
    void setup() {}         // run before every feature method
    void cleanup() {}       // run after every feature method
    void setupSpec() {}     // run before the first feature method
    void cleanupSpec() {}   // run after the last feature method

    private MockCoreRepository mockCoreRepository = Mock()
    private BootstrapExampleProperties bootstrapExampleProperties = Mock()
    private RepositoryBootstrapTask repositoryBootstrapTask = new RepositoryBootstrapTask(
            mockCoreRepository,
            bootstrapExampleProperties)

    // In groovy you can name your functions with strings.
    // Strings in Groovy should always use single quotes '...'
    void 'This is an example test'() {
        given: 'a set of parameters'
        var example = new ExampleEntity(
                id: UUID.fromString('00000000-0000-0000-0000-000000000000'),
                name: 'MockExample')

        when: 'this code is executed'
        repositoryBootstrapTask.run(null)

        then: 'these mocks will be called'
        // number of calls * nameOfMock.methodName(argument validation) >> method stub return value
        1 * bootstrapExampleProperties.getExamples() >> [example] // square brackets [...] are Groovy's list syntax
        1 * mockCoreRepository.findByName('MockExample') >> Optional.empty()
        1 * mockCoreRepository.save(example) >> example

        // 'and' can be used to extend any [given, when, then] to separate chunks of code
        and: 'the result is verified'
        // Code under test returns void so there isn't any result to assert in this case
        /* Example verification code:
        verifyAll(result) {
            // all of these are equivalent within this block
            result.field == expectedValue // Not preferred, redundant reference to `result` already covered by closure
            it.field == expectedValue
            field == expectedValue
        }
        */
    }
}
