package com.capco.bank.mockcore.bootstrap;


import com.capco.bank.mockcore.RepositoryBootstrapTask;
import com.capco.bank.mockcore.config.BootstrapExampleProperties;
import com.capco.bank.mockcore.repository.MockCoreRepository;
import com.capco.bank.mockcore.repository.model.ExampleEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
class ExampleTest {

    @Mock
    private MockCoreRepository repository;
    @Mock
    private BootstrapExampleProperties properties;
    @InjectMocks
    private RepositoryBootstrapTask repositoryBootstrapTask;

    @Test
    void testBootstrap() {
        ExampleEntity mockEntity = new ExampleEntity().setName("Entity1").setId(UUID.randomUUID());

        Mockito.when(properties.getExamples()).thenReturn(List.of(mockEntity));
        Mockito.when(repository.findByName(anyString())).thenReturn(Optional.empty());
        Mockito.when(repository.save(any(ExampleEntity.class))).thenReturn(mockEntity);

        repositoryBootstrapTask.run(null);

        Mockito.verify(repository, Mockito.times(1)).save(mockEntity);
    }
}
