package com.capco.bank.mockcore.bootstrap;

import com.capco.bank.mockcore.config.BootstrapExampleProperties;
import com.capco.bank.mockcore.repository.model.ExampleEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest
class PropertiesIT {

    @Autowired
    private BootstrapExampleProperties properties;

	@Test
	void getsBootstrapData() {
		log.info("Running Test");
		List<ExampleEntity> examples = properties.getExamples();
		examples.stream().findFirst().ifPresentOrElse(
				example -> assertEquals("Entity1", example.getName()),
				Assertions::fail
		);
	}

}
