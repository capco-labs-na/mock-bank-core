package com.capco.bank.mockcore.web;

import com.capco.bank.mockcore.web.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserController userController;

    @Test
    public void contextLoads() throws Exception {
        assertNotNull(userController);
    }

    @Test
    void whenPostUser_thenStatus200() throws Exception {
        this.mockMvc.perform(post("/users/")
                                .contentType(APPLICATION_JSON)
                        .content("{ \"id\":\"34843b06-a390-4aa1-8bc7-a40e812d2372\" }"))
                .andExpect(status().isCreated());
    }

    @Test
    void whenInvalidId_thenStatus400() throws Exception {
        String requestBody = "{ \"id\":\"123\" }";
        this.mockMvc.perform(post("/users/")
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenEmptyId_thenStatus400() throws Exception {
        String requestBody = "{ \"id\":\" \" }";
        this.mockMvc.perform(post("/users/")
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isBadRequest());
    }
}
