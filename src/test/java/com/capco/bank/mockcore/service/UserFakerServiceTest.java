package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.UserRepository;
import com.capco.bank.mockcore.repository.model.User;
import com.capco.bank.mockcore.util.Utils;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Locale;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class UserFakerServiceTest {
    @Mock
    private UserRepository userRepository;
    private UserFakerService userFakerService;
    private Utils helper;

    @BeforeEach
    void init (){
        Faker faker = new Faker(new Locale("en-us"));
        helper = new Utils(faker);
        userFakerService = new UserFakerService(helper, userRepository);
    }

    @Test
    void shouldCreateMockUser(){
        User dbuser = new User();
        dbuser.setId(UUID.randomUUID());
        dbuser.setExternalId(UUID.randomUUID());
        dbuser.setFirstName("Karon");
        dbuser.setMiddleInitial("A");
        dbuser.setLastName("Mohr,");
        dbuser.setCreatedDT(LocalDate.now());
        dbuser.setJoinedDT(LocalDate.now());
        dbuser.setUpdatedDT(LocalDate.now());
        Mockito.when(userRepository.save(any(User.class))).thenReturn(dbuser);

        User user = this.userFakerService.createUser();

        assertNotNull(user.getId());
        assertNotNull(user.getExternalId());
        assertNotNull(user.getFirstName());
        assertNotNull(user.getMiddleInitial());
        assertNotEquals(user.getMiddleInitial().length(), 0);
        assertNotNull(user.getLastName());
        assertNotNull(user.getJoinedDT());
        assertNotNull(user.getCreatedDT());
        assertNotNull(user.getUpdatedDT());
    }

    @Test
    void shouldCreateMockUserById(){
        User dbuser = new User();
        dbuser.setId(UUID.randomUUID());
        dbuser.setExternalId(UUID.fromString("34843b06-a390-4aa1-8bc7-a40e812d2372"));
        dbuser.setFirstName("Karon");
        dbuser.setMiddleInitial("A");
        dbuser.setLastName("Mohr,");
        dbuser.setCreatedDT(LocalDate.now());
        dbuser.setJoinedDT(LocalDate.now());
        dbuser.setUpdatedDT(LocalDate.now());
        Mockito.when(userRepository.save(any(User.class))).thenReturn(dbuser);

        UUID id = UUID.fromString("34843b06-a390-4aa1-8bc7-a40e812d2372");
        User user = this.userFakerService.createUser(id);

        assertNotNull((user.getId()));
        assertEquals(id, user.getExternalId());
        assertNotNull(user.getFirstName());
        assertNotNull(user.getMiddleInitial());
        assertEquals(user.getMiddleInitial().length(), 1);
        assertNotNull(user.getLastName());
        assertNotNull(user.getJoinedDT());
        assertNotNull(user.getCreatedDT());
        assertNotNull(user.getUpdatedDT());
    }
}
