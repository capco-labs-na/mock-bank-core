package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.UserRepository;
import com.capco.bank.mockcore.repository.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@SpringBootTest
class UserFakerServiceIT {

    @Autowired
    private UserService userFakerService;

    @Autowired
    private UserRepository userRepository;

    @Test
    void contextLoads(){
        assertNotNull(userFakerService);
    }

    @Test
    void userFakerServiceCreatesUser(){
        log.info("Running Test - userFakerServiceCreatesUser");
        User user = userFakerService.createUser();
        Optional<User> repoUser =  userRepository.findByExternalId(user.getExternalId());

        assertEquals(user.getId(), repoUser.get().getId());
    }

    @Test
    void userFakerServiceCreatesUserById(){
        log.info("Running Test - userFakerServiceCreatesUserById");
        UUID id = UUID.fromString("c41f833d-681f-41b9-a395-d0b8d90bb8d4");
        User user = userFakerService.createUser(id);
        Optional<User> repoUser =  userRepository.findByExternalId(user.getExternalId());

        assertEquals(user.getId(), repoUser.get().getId());
    }
}
