package com.capco.bank.mockcore.util;

import com.capco.bank.mockcore.repository.constants.AccountType;
import com.capco.bank.mockcore.repository.constants.BankTransactionStatus;
import com.capco.bank.mockcore.repository.constants.BankTransactionType;
import com.capco.bank.mockcore.repository.constants.PaymentStatus;
import com.capco.bank.mockcore.repository.model.Account;
import com.capco.bank.mockcore.repository.model.BankTransaction;
import com.capco.bank.mockcore.repository.model.Payment;
import com.capco.bank.mockcore.repository.model.User;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class Utils {
    private static final int MAX_RAND_TRANSACTIONS = 50;
    private static final Long MAX_ACCOUNT_AMOUNT = 1000000L;

    private final Faker faker;

    public List<BankTransaction> generateRandomTransactions() {
        List<BankTransaction> transactions = new ArrayList<>();
        int numOfTransactions = new Random().nextInt(MAX_RAND_TRANSACTIONS) + 1;
        for (int i = 0; i <= numOfTransactions; i++) {
            BankTransaction transaction = new BankTransaction();
            transaction.setId(UUID.randomUUID());
            transaction.setCreate_ts(LocalDateTime.now());
            transaction.setUpdate_ts(LocalDateTime.now());
            transaction.setMerchant_nm(faker.company().name());
            transaction.setTransaction_amt(faker.commerce().price());
            transaction.setTransaction_typ(faker.options().option(BankTransactionType.values()).toString());
            transaction.setTransaction_status(faker.options().option(BankTransactionStatus.values()).toString());
            transactions.add(transaction);
        }
        return transactions;
    }

    public User generateRandomUser() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setFirstName(faker.name().firstName());
        user.setMiddleInitial(faker.letterify("?").toUpperCase());
        user.setLastName(faker.name().lastName());
        user.setFirstName(faker.name().firstName());
        user.setMiddleInitial(faker.letterify("?").toUpperCase());
        user.setLastName(faker.name().lastName());
        user.setCreatedDT(LocalDate.now());
        user.setJoinedDT(LocalDate.now());
        user.setUpdatedDT(LocalDate.now());
        return user;
    }

    public Account generateRandomAccount() {
        String amount = String.valueOf(getNumberBetween(0, MAX_ACCOUNT_AMOUNT));
        Account account = new Account();
        account.setId(UUID.randomUUID());
        account.setAccountType(faker.options().option(AccountType.values()).toString());
        account.setBalanceAmt(amount);
        account.setAvailableAmt(amount);
        account.setOpenDt(LocalDateTime.now());
        account.setCreatedDt(LocalDate.now());
        account.setUpdatedDt(LocalDate.now());
        return account;
    }

    public List<Payment> generateRandomPayments() {
        List<Payment> paymentList = new ArrayList<>();
        int NUMBER_OF_PAYMENTS = getNumberBetween(0, 10);
        for (int i = 0; i < NUMBER_OF_PAYMENTS; i++) {
            Payment payment = new Payment();
            payment.setId(UUID.randomUUID());
            payment.setMerchantNm(faker.company().name());
            payment.setPaymentsAmt(faker.commerce().price());
            payment.setOriginId(UUID.randomUUID().toString());
            payment.setRecipientId(UUID.randomUUID().toString());
            payment.setPostingDt(LocalDate.now());
            payment.setCreatedDt(LocalDate.now());
            payment.setUpdatedDt(LocalDate.now());
            payment.setPaymentStatus(faker.options().option(PaymentStatus.values()).toString());
            paymentList.add(payment);
        }
        return paymentList;
    }

    public int getNumberBetween(int min, int max) {
        return faker.number().numberBetween(min, max);
    }

    public Long getNumberBetween(int min, Long max) {
        return faker.number().numberBetween(min, max);
    }
}
