package com.capco.bank.mockcore;

import com.capco.bank.mockcore.config.BootstrapExampleProperties;
import com.capco.bank.mockcore.repository.MockCoreRepository;
import com.capco.bank.mockcore.repository.model.ExampleEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class RepositoryBootstrapTask implements ApplicationRunner {
    // Example class to programmatically populate database

    // @RequiredArgsConstructor + "final" modifier creates a constructor that automatically @Autowires this property
    private final MockCoreRepository mockCoreRepository;
    private final BootstrapExampleProperties bootstrapProperties;

    @Override
    public void run(ApplicationArguments args) {
        List<ExampleEntity> examples = bootstrapProperties.getExamples();
        log.info("Bootstrapping database");
        examples.forEach(exampleEntity -> {
            if (mockCoreRepository.findByName(exampleEntity.getName()).isEmpty()) {
                ExampleEntity savedExample = mockCoreRepository.save(exampleEntity);
                log.info("Saved {} with id {}", savedExample.getName(), savedExample.getId());
            }
        });
    }
}
