package com.capco.bank.mockcore.config;

import com.github.javafaker.Faker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Locale;
@Configuration
public class FakerConfig {
    @Bean
    public Faker getUsEnFaker(){
        return new Faker(new Locale("en-us"));
    }
}
