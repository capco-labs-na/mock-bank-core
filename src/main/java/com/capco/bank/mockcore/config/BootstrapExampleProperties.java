package com.capco.bank.mockcore.config;

import com.capco.bank.mockcore.repository.model.ExampleEntity;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties("bootstrap")
public class BootstrapExampleProperties {
    // List of examples to pre-populate database
    private List<ExampleEntity> examples;
}
