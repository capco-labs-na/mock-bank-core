package com.capco.bank.mockcore.repository.constants;

public enum PaymentStatus {
    PENDING,
    POSTED,
    ACCEPTED,
    REJECTED;
}
