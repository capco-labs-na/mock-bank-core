package com.capco.bank.mockcore.repository.constants;

public enum BankTransactionType {
    DEBIT,
    CREDIT;
}
