package com.capco.bank.mockcore.repository.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
public class User {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

	@Column
    private UUID externalId;

    @Column
    private String firstName;

    @Column
    private String middleInitial;

    @Column
    private String lastName;

    @Column
    private LocalDate joinedDT;

    @Column
    private LocalDate createdDT;

    @Column
    private LocalDate updatedDT;
}
