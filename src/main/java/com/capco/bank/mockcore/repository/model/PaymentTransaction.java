package com.capco.bank.mockcore.repository.model;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Id;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.util.UUID;

public class PaymentTransaction {
    @Id
    @Column(columnDefinition="BINARY(16)")
    @GenericGenerator(name="uuid2",strategy="uuid2")
    @GeneratedValue(generator="uuid2")
    private UUID id;

    @ManyToOne
    @JoinColumn(name="toBeNamed")//placeholder name
    private Payment payment;//payment table needs merging into master

    @ManyToOne
    @JoinColumn(name="ID")//
    private BankTransaction bankTransaction;

    @Column
    private LocalDate create_DT;
    @Column
    private LocalDate update_DT;
}
