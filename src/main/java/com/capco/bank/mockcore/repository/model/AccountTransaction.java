package com.capco.bank.mockcore.repository.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
public class AccountTransaction {
    @Id
    @Column(columnDefinition="BINARY(16)")
    @GenericGenerator(name="uuid2",strategy="uuid2")
    @GeneratedValue(generator="uuid2")
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "accountID")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "transactionID")
    private BankTransaction transaction;

    @Column
    private LocalDate createdDt;

    @Column
    private LocalDate updatedDt;
}
