package com.capco.bank.mockcore.repository.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
public class Account {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    @Column
    private String accountType;

    @Column
    private LocalDateTime openDt;

    @Column
    private String balanceAmt;

    @Column
    private String availableAmt;

    @Column
    private LocalDate updatedDt;

    @Column
    private LocalDate createdDt;
}
