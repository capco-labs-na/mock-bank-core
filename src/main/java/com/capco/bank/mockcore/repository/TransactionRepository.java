package com.capco.bank.mockcore.repository;

import com.capco.bank.mockcore.repository.model.BankTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TransactionRepository extends JpaRepository<BankTransaction, UUID> {
}
