package com.capco.bank.mockcore.repository;

import com.capco.bank.mockcore.repository.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserAccountRepository extends JpaRepository<UserAccount, UUID> {
}
