package com.capco.bank.mockcore.repository;

import com.capco.bank.mockcore.repository.model.ExampleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MockCoreRepository extends JpaRepository<ExampleEntity, UUID> {
    Optional<ExampleEntity> findByName(String name);
}
