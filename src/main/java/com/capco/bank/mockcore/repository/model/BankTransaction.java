package com.capco.bank.mockcore.repository.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
public class BankTransaction {

    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2",strategy="uuid2")
    @Id
    @Column(columnDefinition="BINARY(16)",name="ID")
    private UUID id;
    
    @Column
    private String merchant_nm;

    @Column
    private String transaction_amt;

    @Column
    private String transaction_typ;

    @Column
    private String transaction_status;

    // @LocalDate
    @Column
    private LocalDateTime create_ts;

    @Column
    private LocalDateTime update_ts;
}
