package com.capco.bank.mockcore.repository;

import com.capco.bank.mockcore.repository.model.AccountTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, UUID> {
}
