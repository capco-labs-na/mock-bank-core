package com.capco.bank.mockcore.repository.constants;

public enum AccountType {
    CHECKING,
    SAVINGS
}
