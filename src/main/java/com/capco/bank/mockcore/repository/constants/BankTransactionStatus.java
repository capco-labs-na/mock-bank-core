package com.capco.bank.mockcore.repository.constants;

public enum BankTransactionStatus {
    PENDING,
    POSTED,
    ACCEPTED
}
