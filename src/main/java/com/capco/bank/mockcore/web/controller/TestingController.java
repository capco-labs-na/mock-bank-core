package com.capco.bank.mockcore.web.controller;

import com.capco.bank.mockcore.repository.model.AccountTransaction;
import com.capco.bank.mockcore.repository.model.Payment;
import com.capco.bank.mockcore.repository.model.UserAccount;
import com.capco.bank.mockcore.service.AccountTransactionService;
import com.capco.bank.mockcore.service.PaymentService;
import com.capco.bank.mockcore.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class TestingController {
    private final AccountTransactionService accountTransactionService;
    private final UserAccountService userAccountService;
    private final PaymentService paymentService;

    @PostMapping(path = "accountTransaction")
    public ResponseEntity<List<AccountTransaction>> createAccountWithTransactions() {
        try {
            List<AccountTransaction> accountTransactionList = accountTransactionService.createAccountWithTransactions();
            return ResponseEntity.status(HttpStatus.CREATED).body(accountTransactionList);

        } catch (NullPointerException e) {
            log.error("Bad Request: ", e);
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping(path = "userAccounts")
    public ResponseEntity<List<UserAccount>> createUserWithAccounts() {
        try {
            List<UserAccount> userAccountList = userAccountService.createUserWithAccountsWithTransactions();
            return ResponseEntity.status(HttpStatus.CREATED).body(userAccountList);

        } catch (NullPointerException e) {
            log.error("Bad Request: ", e);
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping(path = "payments")
    public ResponseEntity<List<Payment>> createPayments() {
        try {
            List<Payment> paymentList = paymentService.createPayments();
            return ResponseEntity.status(HttpStatus.CREATED).body(paymentList);

        } catch (NullPointerException e) {
            log.error("Bad Request: ", e);
            return ResponseEntity.badRequest().build();
        }
    }
}
