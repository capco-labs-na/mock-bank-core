package com.capco.bank.mockcore.web.controller;

import com.capco.bank.mockcore.repository.model.User;
import com.capco.bank.mockcore.service.UserService;
import com.capco.bank.mockcore.web.request.CreateByIdRequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path ="users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userFakerService;

    @PostMapping
    public ResponseEntity<User> createUserById(@RequestBody CreateByIdRequestBody createUserRequestBody) {
        try {
            User user = userFakerService.createUser(createUserRequestBody.getId());
            return new ResponseEntity<>(user, HttpStatus.CREATED);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
}
