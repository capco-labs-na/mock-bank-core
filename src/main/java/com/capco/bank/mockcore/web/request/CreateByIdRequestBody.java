package com.capco.bank.mockcore.web.request;


import lombok.Data;

import java.util.UUID;

@Data
public class CreateByIdRequestBody {
    private UUID id;
}
