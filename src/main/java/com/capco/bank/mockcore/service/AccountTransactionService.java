package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.model.Account;
import com.capco.bank.mockcore.repository.model.AccountTransaction;

import java.util.List;

public interface AccountTransactionService {
    List<AccountTransaction> createAccountWithTransactions();
    List<AccountTransaction> createTransactionsForAccount(Account account);
}
