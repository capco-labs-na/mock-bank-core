package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.model.Account;

public interface AccountService {
    Account createAccount();
}
