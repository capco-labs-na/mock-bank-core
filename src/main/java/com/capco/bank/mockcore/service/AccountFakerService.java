package com.capco.bank.mockcore.service;


import com.capco.bank.mockcore.repository.AccountRepository;
import com.capco.bank.mockcore.repository.constants.AccountType;
import com.capco.bank.mockcore.repository.model.Account;
import com.capco.bank.mockcore.util.Utils;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class AccountFakerService implements AccountService {

    private final AccountRepository accountRepository;
    private final Utils helper;

    /**
     *
     * @return new random-generated account that has been saved to DB
     */
    @Override
    public Account createAccount() {
        Account account = helper.generateRandomAccount();
        return accountRepository.save(account);
    }

}
