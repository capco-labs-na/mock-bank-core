package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.model.User;

import java.util.UUID;

public interface UserService {
    User createUser(UUID uuid);
    User createUser();
}
