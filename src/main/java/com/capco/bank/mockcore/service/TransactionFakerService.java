package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.AccountTransactionRepository;
import com.capco.bank.mockcore.repository.TransactionRepository;
import com.capco.bank.mockcore.repository.model.BankTransaction;
import com.capco.bank.mockcore.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.*;


@Service
@RequiredArgsConstructor
public class TransactionFakerService implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final Utils helper;

    public List<BankTransaction> createTransactions(){
        List<BankTransaction> transactionList = helper.generateRandomTransactions();
        return transactionRepository.saveAll(transactionList);
    }

    public List<BankTransaction> getTransactionsByAccount(UUID accountId) {
        return null;
    }

}
