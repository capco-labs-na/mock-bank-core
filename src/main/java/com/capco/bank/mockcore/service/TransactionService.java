package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.model.BankTransaction;

import java.util.List;
import java.util.UUID;


public interface TransactionService {
    List<BankTransaction> createTransactions();
    List<BankTransaction> getTransactionsByAccount(UUID accountId);
}
