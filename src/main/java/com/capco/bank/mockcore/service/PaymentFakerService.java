package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.PaymentRepository;
import com.capco.bank.mockcore.repository.model.Payment;
import com.capco.bank.mockcore.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PaymentFakerService implements PaymentService{
    private final PaymentRepository paymentRepository;
    private final Utils helper;

    /**
     * @return List of 1-10 random payments
     */
    @Override
    public List<Payment> createPayments() {
        List<Payment> paymentList = helper.generateRandomPayments();
        return paymentRepository.saveAll(paymentList);
    }
}
