package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.model.User;
import com.capco.bank.mockcore.repository.model.UserAccount;

import java.util.List;

public interface UserAccountService {
    List<UserAccount> createUserWithAccountsWithTransactions();
    List<UserAccount> createAccountsWithTransactionsForUser(User user);
}
