package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.model.Payment;

import java.util.List;

public interface PaymentService {
    List<Payment> createPayments();
}
