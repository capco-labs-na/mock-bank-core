package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.AccountTransactionRepository;
import com.capco.bank.mockcore.repository.model.Account;
import com.capco.bank.mockcore.repository.model.AccountTransaction;
import com.capco.bank.mockcore.repository.model.BankTransaction;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class AccountTransactionFakerService implements AccountTransactionService {
    private final AccountService accountService;
    private final TransactionService transactionService;
    private final AccountTransactionRepository accountTransactionRepository;

    /**
     *  When save() fails, will rollback changes
     *
     * @return List of BankTransactions
     */
    @Transactional(rollbackOn = Exception.class)
    public List<AccountTransaction> createAccountWithTransactions() {
        Account account = accountService.createAccount();
        return createTransactionsForAccount(account);
    }

    /**
     *  When save() fails, will rollback changes
     *
     * @return List of AccountTransaction link records
     */
    @Transactional(rollbackOn = Exception.class)
    public List<AccountTransaction> createTransactionsForAccount(Account account) {
        //create transactions
        List<BankTransaction> transactionList = transactionService.createTransactions();

        //associate transactions with account
        List<AccountTransaction> accountTransactionList = new ArrayList<>();
        for (BankTransaction transaction : transactionList) {
            AccountTransaction accountTransaction = new AccountTransaction();

            accountTransaction.setId(UUID.randomUUID());
            accountTransaction.setTransaction(transaction);
            accountTransaction.setAccount(account);
            accountTransaction.setCreatedDt(LocalDate.now());
            accountTransaction.setUpdatedDt(LocalDate.now());
            accountTransactionRepository.save(accountTransaction);

            accountTransactionList.add(accountTransaction);
        }

        return accountTransactionList;
    }
}
