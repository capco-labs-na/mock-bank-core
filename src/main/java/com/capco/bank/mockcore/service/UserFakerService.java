package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.UserRepository;
import com.capco.bank.mockcore.repository.model.User;
import com.capco.bank.mockcore.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserFakerService implements UserService {
    private final Utils helper;
    private final UserRepository userRepository;

    @Override
    public User createUser() {
        User user = helper.generateRandomUser();
        user.setExternalId(UUID.randomUUID());
        return userRepository.save(user);
    }

    public User createUser(UUID uuid){
        if (uuid == null){
            throw new NullPointerException();
        }

        User user = helper.generateRandomUser();
        user.setExternalId(uuid);
        return userRepository.save(user);
    }


}