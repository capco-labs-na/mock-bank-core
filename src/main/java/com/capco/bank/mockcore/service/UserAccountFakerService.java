package com.capco.bank.mockcore.service;

import com.capco.bank.mockcore.repository.UserAccountRepository;
import com.capco.bank.mockcore.repository.model.*;
import com.capco.bank.mockcore.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class UserAccountFakerService implements UserAccountService{

    private final UserService userService;
    private final AccountService accountService;
    private final AccountTransactionService accountTransactionService;
    private final UserAccountRepository userAccountRepository;
    private final Utils helper;


    /**
     *  When save() fails, will rollback changes
     *
     * @return List of UserAccounts
     */
    @Transactional(rollbackOn = Exception.class)
    public List<UserAccount> createUserWithAccountsWithTransactions(){
        User user = userService.createUser();
        return createAccountsWithTransactionsForUser(user);
    }

    /**
     *  When save() fails, will rollback changes
     *
     * @return List of UserAccount link records
     */
    @Transactional(rollbackOn = Exception.class)
    public List<UserAccount> createAccountsWithTransactionsForUser(User user){

        //create 1-10 accounts with transactions and associate each with given user
        List<UserAccount> userAccountList = new ArrayList<>();
        int NUMBER_OF_ACCOUNTS = helper.getNumberBetween(0, 10);
        for(int i = 0; i < NUMBER_OF_ACCOUNTS; i++) {
            UserAccount userAccount = new UserAccount();

            Account account = accountService.createAccount();
            List<AccountTransaction> accountTransactionList = accountTransactionService.createTransactionsForAccount(account);

            userAccount.setId(UUID.randomUUID());
            userAccount.setUser(user);
            userAccount.setAccount(account);
            userAccount.setCreate_DT(LocalDate.now());
            userAccount.setUpdate_DT(LocalDate.now());

            userAccountRepository.save(userAccount);
            userAccountList.add(userAccount);
        }

        return userAccountList;
    }
}
